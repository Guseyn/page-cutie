# page-cutie

[![NPM Version][npm-image]][npm-url]

[Page](https://github.com/Guseyn/page) plugin for using [cutie](https://github.com/Guseyn/cutie) library in browser.  It's based on the [Async Tree Pattern](https://github.com/Guseyn/async-tree-patern/blob/master/Async_Tree_Patern.pdf).


For more information visit [cutie](https://github.com/Guseyn/cutie).

## install

`npm install @page-libs/cutie`

## build

`npm run build`

## test

`npm test`

[npm-image]: https://img.shields.io/npm/v/@page-libs/cutie.svg
[npm-url]: https://npmjs.org/package/@page-libs/cutie
